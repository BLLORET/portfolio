import { Component, ElementRef, HostListener, OnInit, QueryList, ViewChildren } from '@angular/core';

@Component({
  selector: 'app-about-me',
  templateUrl: './about-me.component.html',
  styleUrls: ['./about-me.component.sass']
})
export class AboutMeComponent implements OnInit {

  @ViewChildren('revertMobileColors') revertMobileColors!: QueryList<ElementRef<HTMLDivElement>>;
  @ViewChildren('revertMobileDirection') revertMobileDirections!: QueryList<ElementRef<HTMLDivElement>>;
  @ViewChildren('bloc') blocs!: QueryList<ElementRef<HTMLDivElement>>;

  constructor() { }

  ngOnInit(): void {

  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit(): void {
    if (window.innerWidth < 1024) {
      this.revertColors();
      this.addMobileSassClass(this.revertMobileDirections, 'direction-mobile');
      this.addMobileSassClass(this.blocs, 'width-mobile');
    }
  }

  @HostListener('window:resize')
  onResize(): void {
    if (window.innerWidth >= 1024 && this.revertMobileColors.first.nativeElement.classList.contains('white-square')) {
      this.revertColors();
      this.removeMobileSassClass(this.revertMobileDirections, 'direction-mobile');
      this.removeMobileSassClass(this.blocs, 'width-mobile');
    } else if (window.innerWidth < 1024 && this.revertMobileColors.first.nativeElement.classList.contains('blue-square')) {
      this.revertColors();
      this.addMobileSassClass(this.revertMobileDirections, 'direction-mobile');
      this.addMobileSassClass(this.blocs, 'width-mobile');
    }
  }

  /**
   * Revert color of the DOM to make it responsive
   */
  private revertColors(): void {

    this.revertMobileColors.forEach(item => {

      const sassClass: DOMTokenList = item.nativeElement.classList;

      if (sassClass.contains('blue-square')) {
        sassClass.replace('blue-square', 'white-square');
      } else {
        sassClass.replace('white-square', 'blue-square');
      }
    });
  }

  private addMobileSassClass(items: QueryList<ElementRef<HTMLDivElement>>, className: string): void {
    items.forEach(item => {
      item.nativeElement.classList.add(className);
    });
  }

  private removeMobileSassClass(items: QueryList<ElementRef<HTMLDivElement>>, className: string): void {
    items.forEach(item => {
      item.nativeElement.classList.remove(className);
    });
  }
}
