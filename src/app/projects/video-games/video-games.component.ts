import { Component, OnInit } from '@angular/core';
import {Project} from '../../../models/Project';

@Component({
  selector: 'app-video-games',
  templateUrl: './video-games.component.html',
  styleUrls: ['./video-games.component.sass']
})
export class VideoGamesComponent implements OnInit {

  public videoGames: Array<Project> = [
    new Project(
      'Learn&Run',
      '../../assets/pictures/games/learn-and-run.png',
      'Projects.VideoGames.Learn&Run.Description',
      'https://jeanjacquelin.fr/learn&run/learn_and_run_setup.exe', true,
      'Unreal Engine 4.24, Java Spring Boot'
    ) , new Project(
      'Baby Shooting',
      '../../assets/pictures/games/baby-shooting.png',
      'Projects.VideoGames.BabyShooting.Description',
      'https://ldjam.com/events/ludum-dare/46/baby-shooting',
      false,
      'Unity 2019'
    ), new Project(
      'Hunter Hunger',
      '../../assets/pictures/games/hunter-hunger.png',
      'Projects.VideoGames.HunterHunger.Description',
      'https://ldjam.com/events/ludum-dare/45/hunter-hunger',
      false,
      'Unity 2019'
    ), new Project(
      'PokeFight',
      '../../assets/pictures/games/pokefight.png',
      'Projects.VideoGames.PokeFight.Description',
      '../../assets/download/pokefight.apk',
      true,
      'Android Studio/Kotlin, Retrofit, Glide'
    )
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
