import { Component, Input, OnInit} from '@angular/core';
import {Global} from '../../../assets/global';

@Component({
  selector: 'app-project-item',
  templateUrl: './project-item.component.html',
  styleUrls: ['./project-item.component.sass']
})
export class ProjectItemComponent implements OnInit {

  constructor(public global: Global) { }

  @Input() title: string;
  @Input() imgSrc: string;
  @Input() description: string;
  @Input() technologies: string;
  @Input() link: string;
  @Input() download: boolean;

  ngOnInit(): void { }
}
