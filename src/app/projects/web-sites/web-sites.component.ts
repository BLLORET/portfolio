import { Component, OnInit } from '@angular/core';
import {Project} from '../../../models/Project';

@Component({
  selector: 'app-web-sites',
  templateUrl: './web-sites.component.html',
  styleUrls: ['./web-sites.component.sass']
})
export class WebSitesComponent implements OnInit {

  public webSites: Array<Project> = [ new Project(
    'Portfolio',
    '../../assets/pictures/sites/portfolio.png',
    'Projects.WebSites.Portfolio.Description',
    'https://gitlab.com/BLLORET/portfolio/',
    false,
    'Angular10, Bulma, Gitlab-CI, Heroku'
), {
    title: 'Learn&Run',
    imgSrc: '../../assets/pictures/sites/learn-and-run.png',
    description: 'Projects.WebSites.Learn&Run.Description',
    link: 'http://front-end-learn-run.herokuapp.com/',
    download: false,
    technologies: 'Angular10, Bulma, Gitlab-CI, Heroku'
  }];

  constructor() { }

  ngOnInit(): void {
  }

}
