import {Component, OnInit} from '@angular/core';
import {Skills} from '../../models/Skills';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.sass']
})
export class SkillsComponent implements OnInit {

  public skills: Array<Skills> = [
    new Skills('Skills.Languages', [
      '../../assets/pictures/skills/c.png',
      '../../assets/pictures/skills/c-sharp.svg',
      '../../assets/pictures/skills/c++.png',
      '../../assets/pictures/skills/java.png',
      '../../assets/pictures/skills/kotlin.png',
      '../../assets/pictures/skills/html.png',
      '../../assets/pictures/skills/css.png',
      '../../assets/pictures/skills/sass.svg',
      '../../assets/pictures/skills/js.png',
      '../../assets/pictures/skills/ts.png',
      '../../assets/pictures/skills/python.png',
      '../../assets/pictures/skills/sql.png',
      '../../assets/pictures/skills/markdown.png',
      '../../assets/pictures/skills/latex.png',
    ]), new Skills('Skills.GameEngines', [
      '../../assets/pictures/skills/unity.png',
      '../../assets/pictures/skills/unreal-engine.png',
    ]), new Skills('Skills.Frameworks', [
      '../../assets/pictures/skills/dotnet.png',
      '../../assets/pictures/skills/spring-boot.png',
      '../../assets/pictures/skills/angular.png',
      '../../assets/pictures/skills/react.png',
      '../../assets/pictures/skills/django.png',
    ]), new Skills('Skills.Devops', [
      '../../assets/pictures/skills/docker.png',
      '../../assets/pictures/skills/gitlab-ci-cd.png',
      '../../assets/pictures/skills/heroku.png',
    ]), new Skills('Skills.Databases', [
      '../../assets/pictures/skills/postgresql.png',
      '../../assets/pictures/skills/mysql.png',
      '../../assets/pictures/skills/mssql.png',
      '../../assets/pictures/skills/sqlite.png',
      '../../assets/pictures/skills/graphql.png',
    ]), new Skills('Skills.IDE', [
      '../../assets/pictures/skills/vs.png',
      '../../assets/pictures/skills/vscode.png',
      '../../assets/pictures/skills/intellij.png',
      '../../assets/pictures/skills/android-studio.jpg',
      '../../assets/pictures/skills/vim.png',
    ]), new Skills('Skills.SourceCode', [
      '../../assets/pictures/skills/git.png',
      '../../assets/pictures/skills/gitlab.png',
      '../../assets/pictures/skills/github.png',
    ]), new Skills('Skills.Design', [
      '../../assets/pictures/skills/photoshop.svg',
    ]), new Skills('Skills.ProjectManagement', [
      '../../assets/pictures/skills/office.png',
      '../../assets/pictures/skills/project.png'
    ]), new Skills('Skills.BuildSystems', [
      '../../assets/pictures/skills/cmake.png',
    ])
  ];

  constructor() { }

  ngOnInit(): void { }
}
