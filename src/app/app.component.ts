import {Component, HostListener} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {EN_US, FR_FR} from '../assets/constantes/languages';
import {localStorageLanguageKey} from '../assets/constantes/LocalStorage';
import {Global} from '../assets/global';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'portfolio';

  constructor(private translate: TranslateService, private global: Global) {
    global.isMobile = window.innerWidth < 1024;

    translate.addLangs([EN_US, FR_FR]);

    // Check preference in memory
    let defaultLang: string = localStorage.getItem(localStorageLanguageKey);

    // Detect the browser language by default if no preferences
    if (defaultLang === null) {
      defaultLang = EN_US;
      const browserLang: string = translate.getBrowserLang();

      if (browserLang.includes('fr')) {
        defaultLang = FR_FR;
      }
      localStorage.setItem(localStorageLanguageKey, defaultLang);
    }

    translate.setDefaultLang(defaultLang);
    translate.use(defaultLang);
  }

  @HostListener('window:resize')
  onResize(): void {
    if (window.innerWidth < 1024 && !this.global.isMobile) {
      this.global.isMobile = true;
    } else if (window.innerWidth >= 1024 && this.global.isMobile){
      this.global.isMobile = false;
    }
  }
}
