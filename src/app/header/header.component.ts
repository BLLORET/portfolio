import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {EN_US, ENGLISH, FR_FR, FRENCH} from '../../assets/constantes/languages';
import {localStorageLanguageKey} from '../../assets/constantes/LocalStorage';
import {Global} from '../../assets/global';
import {faGamepad, faGlobe, faTools, faUser} from '@fortawesome/free-solid-svg-icons';
import {faGithub, faGitlab} from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  public isDropdownProjectsHovered = false;
  public isDropdownLanguageHovered = false;

  @ViewChild('navbarMenu', { static: true }) navbarMenu: ElementRef<HTMLDivElement>;

  public readonly ENGLISH: string = ENGLISH;
  public readonly FRENCH: string = FRENCH;
  public currentLanguage: string;

  // icons
  public faUser = faUser;
  public faTools = faTools;
  public faGamepad = faGamepad;
  public faGlobe = faGlobe;
  public faGitHub = faGithub;
  public faGitLab = faGitlab;

  constructor(private translate: TranslateService, public global: Global) {
    this.currentLanguage = this.getCurrentLanguage();
  }

  /**
   * Determine the current language of the website
   */
  private getCurrentLanguage(): string {
    switch (this.translate.currentLang) {
      case FR_FR:
        return FRENCH;
      case EN_US:
      default:
        return ENGLISH;
    }
  }

  ngOnInit(): void { }

  /**
   * Add tabs in burger
   */
  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit(): void {
    const burger = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
    if (burger.length > 0) {
      burger.forEach(element => {
        element.addEventListener('click', () => {

          // Get the target from the "data-target" attribute
          const target = element.dataset.target;
          const $target = document.getElementById(target);

          // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
          element.classList.toggle('is-active');
          $target.classList.toggle('is-active');
        });
      });
    }
  }

  /**
   * Switch the boolean value that detect that dropdown is hover projects elements
   */
  public switchIsDropdownProjectsHovered(): void {
    this.isDropdownProjectsHovered = !this.isDropdownProjectsHovered;
  }

  /**
   * Switch the boolean value that detect that dropdown is hover languages elements
   */
  public switchIsDropdownLanguageHovered(): void {
    this.isDropdownLanguageHovered = !this.isDropdownLanguageHovered;
  }

  /**
   * Change the current language of the application
   * @param language The new language to set
   */
  public changeLanguage(language: string): void {
    this.currentLanguage = language;
    switch (language) {
      case FRENCH:
        this.translate.use(FR_FR);
        localStorage.setItem(localStorageLanguageKey, FR_FR);
        break;
      case ENGLISH:
      default:
        this.translate.use(EN_US);
        localStorage.setItem(localStorageLanguageKey, EN_US);
    }
  }
}
