export class Project {
  constructor(public title: string,
              public imgSrc: string,
              public description: string,
              public link: string,
              public download: boolean,
              public technologies: string) { }
}
